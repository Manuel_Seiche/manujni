# README #

[JNI] JavaFX - C++

### What is this repository for? ###

* Eclipse-Projekt fuer Beispielinteraktion zwischen C++ und JavaFX ueber JNI (Java Native Interface)
* V 0.1

### How do I get set up? ###

* Eclipse - Eierlegende Wollmilchsau installieren
* MinGW 64 Bit download
* MinGW\bin zu PATH-Systemvariable hinzufuegen
* Eclipse restart (sofern offen gewesen)
* Genutztes JDK8 zu ManuJNI/jni/makefile JDK_HOME hinzufuegen
* ManuJNI/jni/makefile - Target - clean ausfuehren
* ManuJNI/jni/makefile - Target - all ausfuehren
* Run As -> Run Configurations -> VM-Arguments: -Djava.library.path=jni

### Who do I talk to? ###

* Manuel Seiche