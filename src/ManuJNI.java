import de.mseiche.jni.JNIIn;
import de.mseiche.jni.JNIOut;
import de.mseiche.jni.ui.ManuUI;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 * Diese Klasse startet die Ausf&uuml;hrung der Anwendung und initiiert die
 * Anzeige der GUI. Desweiteren sind alle native-Methods hier enthalten.
 * 
 * @author Manuel Seiche
 * @since 15.04.2017
 */
public class ManuJNI extends Application implements JNIOut, JNIIn {
	static {
		// manu.dll laden
		System.loadLibrary("manu");
	}

	/**
	 * Einstiegspunkt des Programms
	 * 
	 * @param args
	 *            Kommandozeilenargumente... (unused)
	 */
	public static void main(String[] args) {
		launch(args);
	}

	/**
	 * Die Oberfl&auml;che der Anwendung
	 */
	private ManuUI ui;

	@Override
	public void start(Stage primaryStage) throws Exception {
		ui = new ManuUI(primaryStage, this);
		ui.show();
	}

	/**
	 * @see JNIOut#initialize()
	 */
	public native void initialize();

	/**
	 * @see JNIOut#sayHello()
	 */
	public native void sayHello();

	/**
	 * @see JNIOut#shutdownPC()
	 */
	public native void shutdownPC();

	/**
	 * @see JNIOut#tayfun()
	 */
	public native void tayfun();

	/**
	 * @see JNIIn#omfg()
	 */
	public void omfg(String text) {
		ui.getSayHelloLabel().setText("\"" + text + "\" from C++");
	}

	@Override
	public void error(String text) {
		ui.error(text);
	}

	@Override
	public void info(String text) {
		ui.info(text);
	}
}
