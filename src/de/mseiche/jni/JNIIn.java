package de.mseiche.jni;

import de.mseiche.jni.ui.ManuUI;

/**
 * Enth&auml;lt alle Methoden, welche aus C++ heraus aufgerufen werden
 * 
 * @author Manuel Seiche
 * @since 15.04.2017
 */
public interface JNIIn {

	/**
	 * Wird aus C++ aufgerufen, um einen Text auf unserem {@link ManuUI UI}
	 * anzuzeigen<br>
	 * <b>Einstiegspunkt</b>: {@link JNIOut#sayHello()}
	 * 
	 * @param text
	 *            Textparameter aus C++
	 */
	public void omfg(String text);

	/**
	 * Wird aus C++ aufgerufen, um einen Fehlertext auf dem {@link ManuUI}
	 * anzuzeigen
	 * 
	 * @param text
	 *            Anzuzeigender Fehlertext
	 */
	public void error(String text);

	/**
	 * Wird aus C++ aufgerufen, um einen Informationstext auf dem {@link ManuUI}
	 * anzuzeigen
	 * 
	 * @param text
	 *            Anzuzeigender Infotext
	 */
	public void info(String text);
}
