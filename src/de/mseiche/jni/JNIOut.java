package de.mseiche.jni;

import de.mseiche.jni.ui.ManuUI;

/**
 * Enth&auml;lt alle Methoden, welche &uuml;ber Java aufgerufen werden, um C++ -
 * Logik zu verwenden.
 * 
 * @author Manuel Seiche
 * @since 15.04.2017
 */
public interface JNIOut {

	/**
	 * Initialisiert unser C++ - JNI-Umfeld
	 */
	public void initialize();

	/**
	 * Nutzt C++, um auf unserem {@link ManuUI#getSayHelloLabel() Hello-Label}
	 * einen Text anzuzeigen, der aus C++ gesetzt wird.<br>
	 * <b>R&uuml;ckkehrpunkt</b>: {@link JNIIn#omfg(String)}
	 */
	public void sayHello();

	/**
	 * Nutzt C++, um den PC zu beenden
	 */
	public void shutdownPC();

	/**
	 * Tayfun!
	 */
	public void tayfun();
}
