package de.mseiche.jni.ui;

import de.mseiche.jni.JNIIn;
import de.mseiche.jni.JNIOut;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.stage.Screen;
import javafx.stage.Stage;

/**
 * Die Anzeigeklasse der Anwendung. Eine Referenz darauf wird in unserer
 * JNI-Interface-Impl im default package gehalten.
 * 
 * @author Manuel Seiche
 * @since 15.04.2017
 */
public class ManuUI extends BorderPane {

	/**
	 * Das Hauptanzeigefenster, welches uns im Konstruktor &uuml;bergeben wird
	 */
	private Stage primaryStage;

	/**
	 * Dieser {@link ScrollPane} wird der {@link #outputTextFlow} zugewiesen
	 */
	private ScrollPane outputScrollPane;

	/**
	 * Wir leiten {@link System#err}, sowie {@link System#out} auf diesen
	 * {@link TextFlow} weiter
	 */
	private TextFlow outputTextFlow;

	/**
	 * {@link HBox} f&uuml;r {@link #sayHelloButton} und {@link #sayHelloLabel}
	 */
	private HBox sayHelloHBox;

	/**
	 * {@link Label}, welches durch {@link JNIIn#omfg(String)} beschrieben wird
	 */
	private Label sayHelloLabel;

	/**
	 * {@link Button}, welcher JNI nutzt, um C++ zu sagen, dass es in unserer
	 * Java-UI einen Text setzen soll
	 */
	private Button sayHelloButton;

	/**
	 * {@link HBox} f&uuml;r:
	 * <ul>
	 * <li>{@link #shutdownPCButton}</li>
	 * <li>{@link #tayfunButton}</li>
	 * </ul>
	 */
	private HBox bottomHBox;

	/**
	 * {@link Button}! Tayfun!
	 */
	private Button tayfunButton;

	/**
	 * {@link Button}, welcher JNI nutzt, um C++ den PC beenden zu lassen
	 */
	private Button shutdownPCButton;

	/**
	 * Der Zugriff auf C++ erfolgt &uuml;ber den Aufruf von Methoden auf diesem
	 * {@link JNIOut}
	 */
	private JNIOut jniInterface;

	/**
	 * Erzeugt das {@link ManuUI} mit dem zu verwendenden {@link JNIOut} als
	 * Parameter
	 * 
	 * @param primaryStage
	 *            JavaFX-Hauptanzeigefenster
	 * @param jniInterface
	 *            {@link JNIOut}, auf welchem wir die in C++ implementierte
	 *            Logik aufrufen.
	 */
	public ManuUI(Stage primaryStage, JNIOut jniInterface) {
		super();
		this.primaryStage = primaryStage;
		this.jniInterface = jniInterface;
		initialize();
	}

	/**
	 * Baut den Child-Tree dieser {@link Pane} auf
	 */
	private void initialize() {
		getStyleClass().add("manu-ui");
		setTop(getSayHelloHBox());
		setCenter(getOutputScrollPane());
		setBottom(getBottomHBox());
		widthProperty().addListener(inv -> updateInsets());
		heightProperty().addListener(inv -> updateInsets());
		primaryStage.setOnShown(event -> jniInterface.initialize());
	}

	/**
	 * F&uuml;gt einen Fehler-Text zum {@link #outputTextFlow} hinzu
	 * 
	 * @param error
	 *            Fehlermeldung
	 */
	public void error(String error) {
		flow(error, "-fx-fill:#880000;-fx-font-size:1.2em;");
	}

	/**
	 * F&uuml;gt einen Info-Text zum {@link #outputTextFlow} hinzu
	 * 
	 * @param info
	 *            Infomeldung
	 */
	public void info(String info) {
		flow(info, "-fx-fill:#000088;-fx-font-size:1.2em;");
	}

	/**
	 * F&uuml;gt einen {@link Text} zum {@link #outputTextFlow} hinzu
	 * 
	 * @param text
	 *            Hinzuzuf&uuml;gender String
	 * @param style
	 *            CSS-Style des hier erzeugten {@link Text} - Nodes
	 */
	private void flow(String text, String style) {
		Text textNode = new Text(text + '\n');
		textNode.setStyle(style);
		getOutputTextFlow().getChildren().add(textNode);
	}

	/**
	 * Zeigt das UI an
	 */
	public void show() {
		Rectangle2D screenSize = Screen.getPrimary().getBounds();
		Scene scene = new Scene(this, screenSize.getWidth() * 0.35D, screenSize.getHeight() * 0.5D);
		scene.getStylesheets().add(getClass().getResource("/de/mseiche/jni/ui/style.css").toExternalForm());
		primaryStage.setMinWidth(scene.getWidth() * 0.75D);
		primaryStage.setMinHeight(scene.getHeight() * 0.75D);
		primaryStage.setScene(scene);
		primaryStage.setTitle("ManuJNI-FX");
		primaryStage.show();
	}

	/**
	 * Sorgt daf&uuml;r, dass die Insets dieser {@link BorderPane} angepasst
	 * werden, wenn das Fenster resized wird.
	 */
	private void updateInsets() {
		double insets = getHeight() * 0.05D;
		BorderPane.setMargin(getSayHelloHBox(), new Insets(insets));
		BorderPane.setMargin(getOutputScrollPane(), new Insets(0D, insets, 0D, insets));
		BorderPane.setMargin(getBottomHBox(), new Insets(insets));
	}

	/**
	 * Gibt die {@link ScrollPane} zur&uuml;ck, welcher unser
	 * {@link #outputTextFlow} zugewiesen wird
	 * 
	 * @return {@link #outputScrollPane}
	 */
	private ScrollPane getOutputScrollPane() {
		if (outputScrollPane == null) {
			outputScrollPane = new ScrollPane(getOutputTextFlow());
			outputScrollPane.getStyleClass().add("output-scroll-pane");
			outputScrollPane.prefWidthProperty().bind(widthProperty());
			outputScrollPane.prefHeightProperty().bind(heightProperty().subtract(getSayHelloHBox().heightProperty()));
		}
		return outputScrollPane;
	}

	/**
	 * Gibt den {@link TextFlow} zur&uuml;ck, auf welchen unser Console-Output
	 * directed wird
	 * 
	 * @return {@link #outputTextFlow}
	 */
	private TextFlow getOutputTextFlow() {
		if (outputTextFlow == null) {
			outputTextFlow = new TextFlow();
			outputTextFlow.getStyleClass().add("output-text-flow");
		}
		return outputTextFlow;
	}

	/**
	 * Gibt die {@link HBox} zur&uuml;ck, welche {@link #sayHelloButton} und
	 * {@link #sayHelloLabel} enth&auml;lt
	 * 
	 * @return {@link #sayHelloHBox}
	 */
	private HBox getSayHelloHBox() {
		if (sayHelloHBox == null) {
			sayHelloHBox = new HBox();
			sayHelloHBox.spacingProperty().bind(widthProperty().multiply(0.05D));
			sayHelloHBox.setAlignment(Pos.CENTER_LEFT);
			sayHelloHBox.prefWidthProperty().bind(widthProperty());
			sayHelloHBox.getChildren().addAll(getSayHelloButton(), getSayHelloLabel());
		}
		return sayHelloHBox;
	}

	/**
	 * Der {@link Button}, welcher {@link JNIOut#sayHello()} aufruft
	 * 
	 * @return {@link #sayHelloButton}
	 */
	private Button getSayHelloButton() {
		if (sayHelloButton == null) {
			sayHelloButton = new Button("sayHello()");
			sayHelloButton.setOnAction(e -> jniInterface.sayHello());
		}
		return sayHelloButton;
	}

	/**
	 * Die {@link HBox} f&uuml;r {@link #tayfunButton} und
	 * {@link #shutdownPCButton}
	 * 
	 * @return {@link #bottomHBox}
	 */
	private HBox getBottomHBox() {
		if (bottomHBox == null) {
			bottomHBox = new HBox();
			bottomHBox.setAlignment(Pos.CENTER);
			bottomHBox.prefWidthProperty().bind(widthProperty());
			bottomHBox.spacingProperty().bind(widthProperty().multiply(0.05D));
			bottomHBox.getChildren().addAll(getTayfunButton(), getShutdownPCButton());
		}
		return bottomHBox;
	}

	/**
	 * Der {@link Button}, welcher {@link JNIOut#shutdownPC()} aufruft
	 * 
	 * @return {@link #shutdownPCButton}
	 */
	private Button getShutdownPCButton() {
		if (shutdownPCButton == null) {
			shutdownPCButton = new Button("shutdownPC()");
			shutdownPCButton.setOnAction(e -> jniInterface.shutdownPC());
			shutdownPCButton.prefWidthProperty().bind(widthProperty().divide(3));
		}
		return shutdownPCButton;
	}

	/**
	 * Der {@link Button}, welcher {@link JNIOut#tayfun()} aufruft
	 * 
	 * @return {@link #tayfunButton}
	 */
	private Button getTayfunButton() {
		if (tayfunButton == null) {
			tayfunButton = new Button("tayfun()");
			tayfunButton.setOnAction(e -> jniInterface.tayfun());
			tayfunButton.prefWidthProperty().bind(widthProperty().divide(3));
		}
		return tayfunButton;
	}

	/**
	 * Das {@link Label}, welches als Target f&uuml;r {@link JNIOut#sayHello()}
	 * dient
	 * 
	 * @return {@link #sayHelloLabel}
	 */
	public Label getSayHelloLabel() {
		if (sayHelloLabel == null) {
			sayHelloLabel = new Label();
		}
		return sayHelloLabel;
	}
}
