#include "ManuJNI.h"

/*
 * JNI allgemein
 */
#include <jni.h>
#include <jni_md.h>

/*
 * shutdownPC
 */
#include <windows.h>
#pragma comment(lib, "user32.lib")
#pragma comment(lib, "advapi32.lib")

jclass activityClass;
jmethodID omfg;
jmethodID error;
jmethodID info;

/*
 * Shutdown PC
 */
BOOL MySystemShutdown() {
	HANDLE hToken;
	TOKEN_PRIVILEGES tkp;
	// Get a token for this process. 
	if (!OpenProcessToken(GetCurrentProcess(),
	TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken))
		return ( FALSE);

	// Get the LUID for the shutdown privilege.
	LookupPrivilegeValue(NULL, SE_SHUTDOWN_NAME, &tkp.Privileges[0].Luid);
	tkp.PrivilegeCount = 1;  // one privilege to set    
	tkp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;

	// Get the shutdown privilege for this process.
	AdjustTokenPrivileges(hToken, FALSE, &tkp, 0, (PTOKEN_PRIVILEGES) NULL, 0);
	if (GetLastError() != ERROR_SUCCESS)
		return FALSE;
	// Shut down the system and force all applications to close. 
	if (!ExitWindowsEx(EWX_SHUTDOWN | EWX_FORCE,
	SHTDN_REASON_MAJOR_OPERATINGSYSTEM |
	SHTDN_REASON_MINOR_UPGRADE | SHTDN_REASON_FLAG_PLANNED))
		return FALSE;
	//shutdown was successful
	return TRUE;
}

JNIEXPORT void JNICALL Java_ManuJNI_tayfun(JNIEnv *env, jobject thisObj) {
	char* wtfIsThisShit = "Tayfun-Fehleroni!";
	env->CallVoidMethod(thisObj, error, env->NewStringUTF(wtfIsThisShit));
	return;
}

/*
 * Initialisiert unsere globalen Variablen, in denen wir z.B. Method-References halten
 */
JNIEXPORT void JNICALL Java_ManuJNI_initialize(JNIEnv *env, jobject thisObj) {
	activityClass = env->GetObjectClass(thisObj);
	omfg = env->GetMethodID(activityClass, "omfg", "(Ljava/lang/String;)V");
	error = env->GetMethodID(activityClass, "error", "(Ljava/lang/String;)V");
	info = env->GetMethodID(activityClass, "info", "(Ljava/lang/String;)V");
	env->CallVoidMethod(thisObj, info,
			env->NewStringUTF("ManuJNI.cpp initialized"));
	return;
}

/*
 * Schreibt einen String auf ein JavaFX-Oberflaechen-Element
 */
JNIEXPORT void JNICALL Java_ManuJNI_sayHello(JNIEnv *env, jobject thisObj) {
	env->CallVoidMethod(thisObj, omfg,
			env->NewStringUTF("Sent from ManuJNI.cpp!"));
	env->CallVoidMethod(thisObj, info,
			env->NewStringUTF("Java_ManuJNI_sayHello executed"));
	return;
}

/*
 * Schreibt einen String auf ein JavaFX-Oberflaechen-Element
 */
JNIEXPORT void JNICALL Java_ManuJNI_shutdownPC(JNIEnv *env, jobject thisObj) {
	env->CallVoidMethod(thisObj, info, env->NewStringUTF("Shutdown PC"));
	if (!MySystemShutdown()) {
		env->CallVoidMethod(thisObj, error,
				env->NewStringUTF("Shutdown Failed"));
	}
}
